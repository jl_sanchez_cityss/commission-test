# Commission-test

Este proyecto, es un pequeño test en el cual se debe de crear una implementación de la interfaz `CommissionService`.

## DTOs

Todos los DTOs necesarios estan creados y tendrán el campo `id`. Este campo identifica cada objeto de manera única. Las principales entidades para este problema son:

* `SaleDTO`: son los datos relacionados con una venta, aquí tenemos la cantidad monetaria total de la venta y campos que identifican la operación como pueden ser la opción que se vendió. En esta entidad solo puede venir a vacia la información sobre el punto de venta.
* `CustomCommissionDTO`: es la entidad que define una comision, pudiendo ser una cantidad fija o un porcentaje. En el caso de ser porcentaje, el valor vendrá en el rango [0, 1]. También vendrá el orden de la comisión, el cual estará entre 0 y 5. El resto de campos pueden estar o no, cuanto más de estos campos tenga, mas exacta será la comision.

## Problema

En este problema vamos a crear una solución para saber que comision se lleva cada persona participante en la operación de la venta. Estas personas se engancharán a traves de otro sistema con el número del orden.

Para saber esto, se deberá de implementar la función que se encuentra en la interfaz `CommissionService`. A esta función se le pasará la venta que se ha realizado y el listado de todas las posibles comisiones que se pueden aplicar en nuestro sistema y deberá devolver un array donde la posición del mismo define el participante.

### Orden (orderCommission)

Este número indica un participante en el flujo de la venta. Los participantes en el rango [0, 4] se llevarán comisión, si en la lista de comisiones, hay alguna coincidencía con su orden, mientras que el participante 5, se dejará para lo último y será el que cuadre la cuenta.
También se debe de tener en cuenta, que la cantidad siempre debe de estar truncada a 2 cifras. Esto favorece al orden 5 que es el que cuadra la cuenta.

Ejemplo 1, tenemos una venta de 100€ donde el orden 0 se lleva un 60% el 2 se lleva 10€ y el 3 un 10%. El reparto de comisiones quedaría así:

```
0 -> 60€
1 ->  0€
2 -> 10€
3 -> 10€
4 ->  0€
5 -> 20€
```

Ejemplo 2, tenemos una venta de 200€ donde el orden 0 se lleva el 70% el 1 se lleva el 20% y el 3 25€. El reparto de comisiones quedaría así:
```
0 -> 140€
1 ->  40€
2 ->   0€
3 ->  25€
4 ->   0€
5 ->  -5€
```

### Comisión

Cuando se pase la venta y el listado de comisiones se debe de tener en cuenta:

* Solo se aplicará una comisión por participante, la que sea mas exacta.
* Los campos para definir lo exacta que es una comision son: supplierId, productId, optionId, pointOfSale, scopeToDistribute y accountOwner. Además depende de este orden, de manera que si hay dos comisiones para el mismo orden que aplicar, una contiene supplierId y otra optionId, se deberá de aplicar la que esta definida para la opción.
* Una comisión solo se puede aplicar a una venta, si los campos que definen lo exacta que es son iguales a los de la venta o nulos. Por ejemplo una comision que tiene la misma opción que la venta y el resto nulos se podría aplicar a la venta, por el contrario, una comision que tenga todos los campos iguales a la venta pero el identificador de la opción es otro, no podría aplicarse.
* En el caso de los campos de tipo `Long` rellenos, solo se debera de utilizar la función `equals` para saber si son iguales.
* En el caso de los campo de tipo `ScopeDTO`, se debe de tener en cuenta que se pueda aplicar la comision en el caso de ser el mismo scope o el padre del de la venta. Para esto solo debes de utilizar la función `isParent`. Ejemplo `sale.getAccountOwner().isParent(customCommission.getAccountOwner())`.
* En el caso de los campos de tipo `ScopeDTO`, también se debe de tener en cuenta el tipo, de modo que si es de tipo `OPERATIONAL` es mas exacto que `COMPANY` el cual es más exacto que `CHANNEL`. En resumen `OPERATIONAL`>`COMPANY`>`CHANNEL`.
* En la lista no se pasarán dos comisiones que tengan distinto id y el mismo valor en los otros campos.

# Ejemplos
## Calculo de comisión:
**Teniendo**

Lista de CustomCommission
```
[CustomCommissionDTO(id=1, commission=1.5, fixed=true, orderCommission=0, supplierId=1, productId=null, optionId=null, pointOfSale=null, scopeToDistribute=null, accountOwner=null, points=null),
CustomCommissionDTO(id=2, commission=0.1, fixed=false, orderCommission=1, supplierId=null, productId=1, optionId=null, pointOfSale=null, scopeToDistribute=null, accountOwner=null, points=null)]
```
Venta
```
SaleDTO(id=1, totalAmount=20.0, supplierId=1, productId=1, optionId=2, pointOfSale=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL), scopeToDistribute=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL), accountOwner=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL))
```

**Resultado**
La comisión del primer participante es fija, por lo que la cantidad es la indicada en la comision. La del segundo, se obtiene de multiplicar su comision `0.1` por el totalAmount `20`. Para calcular la comisión del último participante, se ha restado al totalAmount, la comisión obtenida por los otros participantes.
```
[1.5, 2, 0, 0, 0, 16.5]
```

## Elección de comisión por exactitud:
**Teniendo**

Lista de CustomCommission
```
[CustomCommissionDTO(id=1, commission=0.1, fixed=false, orderCommission=0, supplierId=1, productId=null, optionId=null, pointOfSale=null, scopeToDistribute=null, accountOwner=null, points=null),
CustomCommissionDTO(id=2, commission=0.3, fixed=false, orderCommission=0, supplierId=null, productId=1, optionId=null, pointOfSale=null, scopeToDistribute=null, accountOwner=null, points=null),
CustomCommissionDTO(id=3, commission=0.5, fixed=false, orderCommission=0, supplierId=null, productId=null, optionId=3, pointOfSale=null, scopeToDistribute=null, accountOwner=null, points=null)]
```
Venta
```
SaleDTO(id=1, totalAmount=20.0, supplierId=1, productId=1, optionId=2, pointOfSale=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL), scopeToDistribute=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL), accountOwner=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL))
```
**Resultado**

Del listado de CustomCommission, la 3 queda eliminada debido a que la opción difiere con la opción de la venta. Tanto la 1 como la 2 pueden ser aplicadas, pero debido a que la 2 es mas exacta, se aplicará dicha comisión. La 2 es más exacta ya que producto es más exacto que proveedor.

La salida queda así:
```
[6, 0, 0, 0, 0, 14]
```

## Elección de comisión por exactitud en scope:
**Teniendo**

Lista de CustomCommission
```
[CustomCommissionDTO(id=1, commission=0.5, fixed=false, orderCommission=0, supplierId=null, productId=null, optionId=null, pointOfSale=null, scopeToDistribute=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL), accountOwner=null, points=null),
CustomCommissionDTO(id=2, commission=0.8, fixed=false, orderCommission=0, supplierId=null, productId=null, optionId=null, pointOfSale=null, scopeToDistribute=ScopeDTO(id=3, channelId=1, companyId=null, name=null, type=COMPANY), accountOwner=null, points=null)]
```
Venta
```
SaleDTO(id=1, totalAmount=20.0, supplierId=1, productId=1, optionId=2, pointOfSale=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL), scopeToDistribute=ScopeDTO(id=8, channelId=1, companyId=3, name=null, type=OPERATIONAL), accountOwner=ScopeDTO(id=1, channelId=null, companyId=null, name=null, type=CHANNEL))
```
**Resultado**

En este caso se pueden aplicar las dos comisiones, pero como la CustomCommision 2, el scopeToDistribute es de typo `COMPANY`, es más exacta que la 1 que es de tipo `CHANNEL`, se aplicará el que tiene `COMPANY`

La salida queda así:
```
[16, 0, 0, 0, 0, 4]
```
