package com.citysightseeing.commissiontest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import com.citysightseeing.commissiontest.service.CommissionService;

import lombok.extern.java.Log;

@Log
@DisplayName("Tests for commission service")
public class CommissionTestApplicationTests {

	private CommissionService commissionService;

	@BeforeEach
	void beforeEachTest(TestInfo testInfo) {
		log.info(() -> String.format("About to execute [%s]", testInfo.getDisplayName()));
	}

	@AfterEach
	void afterEachTest(TestInfo testInfo) {
		log.info(() -> String.format("Finished executing [%s]", testInfo.getDisplayName()));
	}

	@Test
	@DisplayName("Test the commission service initialization")
	public void checkNotNull() {
		assertNotNull(commissionService);
	}

}
