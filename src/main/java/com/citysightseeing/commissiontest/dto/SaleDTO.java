package com.citysightseeing.commissiontest.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class SaleDTO {

	private Long id;
	/**
	 * Money amount of the sale.
	 */
	private double totalAmount;

	/**
	 * Supplier of the product.
	 */
	private Long supplierId;
	/**
	 * Product of the option.
	 */
	private Long productId;
	/**
	 * Option that was sold.
	 */
	private Long optionId;
	/**
	 * Point of the sale. It field can be null.
	 */
	private ScopeDTO pointOfSale;
	/**
	 * Scope that distribute the sale.
	 */
	private ScopeDTO scopeToDistribute;
	/**
	 * Account owner of the sale.
	 */
	private ScopeDTO accountOwner;

}
