package com.citysightseeing.commissiontest.dto;

import com.citysightseeing.commissiontest.dto.enums.ScopeTypeEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class ScopeDTO {

	private Long id;
	private Long channelId;
	private Long companyId;
	private String name;
	private ScopeTypeEnum type;

	public boolean isParent(ScopeDTO scope) {
		Long scopeId = scope.getId();
		return scopeId != null && (scopeId.equals(id) || scopeId.equals(channelId) || scopeId.equals(companyId));
	}
}
