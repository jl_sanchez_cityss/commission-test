package com.citysightseeing.commissiontest.dto.enums;

public enum ScopeTypeEnum {
	CHANNEL, COMPANY, OPERATIONAL
}
