package com.citysightseeing.commissiontest.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class CustomCommisionDTO {

	private Long id;
	/**
	 * Commision to apply
	 */
	private double commission;
	/**
	 * If the commission is fixed, the commission is amount else the commission is
	 * percentage.
	 */
	private boolean fixed;
	/**
	 * The sale could be from 0 to 5 order commission.
	 */
	private int orderCommission;

	// The rest of the fields are to define the commission
	private Long supplierId;
	private Long productId;
	private Long optionId;
	private ScopeDTO pointOfSale;
	private ScopeDTO scopeToDistribute;
	private ScopeDTO accountOwner;

}
